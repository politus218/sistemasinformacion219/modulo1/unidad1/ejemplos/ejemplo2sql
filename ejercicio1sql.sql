﻿-- Ejemplo de la creación de varias tablas para la gestión de productos entre clientes y proveedores

-- Creación de la tabla clientes

CREATE TABLE clientes (
  dnicliente varchar (100),
  nombre varchar (100),
  apellidos varchar (100),
  fechanac date,
  tfno int,
  PRIMARY KEY (dni)
  );
-- Creación de la tabla compran
CREATE TABLE compran (
  dnicliente varchar (100),
  codprod int (100),
  PRIMARY KEY (codprod)
  PRIMARY KEY(codprod,dnicliente), -- creando la clave principal
  UNIQUE KEY (codprod), -- El mismo producto no se puede comprar varias veces con el mismo código
  CONSTRAINT fkcompran FOREIGN KEY (codprod) REFERENCES  (codprod),
  CONSTRAINT fkcompran FOREIGN KEY (codprod) REFERENCES productos (codprod)
  );
-- Creación de la tabla productos
CREATE TABLE productos (
  dniproveedor varchar (100),
  codigo int (100),
  PRIMARY KEY (codigo)
  );
--Creación de la tabla suministran
  nombre varchar (100),
  codigo int (100),
  PRIMARY KEY (codigo)
  PRIMARY KEY(codprod,dniproveedor), -- creando la clave principal
  UNIQUE KEY (codprod), -- NO admite repetidos en productos
  CONSTRAINT fksuministran FOREIGN KEY (codprod) REFERENCES productos(codprod),
  CONSTRAINT fksuministran FOREIGN KEY (codprod) REFERENCES productos (codprod)
  );
-- Creación de la tabla proveedores
CREATE TABLE proveedores (
  dni   varchar (9),
  nombre varchar (100),
  apellidos varchar (100),
  fechanac date,
  tfno int,
  PRIMARY KEY (dni)
  );



